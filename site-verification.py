#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Raphaël Droz, 2018
# General Public License v3 or later
#
# Don't forget to export GOOGLE_APPLICATION_CREDENTIALS environment variable to the json file
#
# Documentation:
# * https://www.google.com/webmasters/tools/home
# * https://developers.google.com/site-verification/libraries
# * https://developers.google.com/api-client-library/python/auth/installed-app
# * https://developers.google.com/resources/api-libraries/documentation/siteVerification/v1/python/latest/siteVerification_v1.webResource.html#insert
# * https://developers.google.com/apis-explorer/#search/verification/siteVerification/v1/siteVerification.webResource.list?_h=1&
# * http://plugins.svn.wordpress.org/official-google-site-verification-plugin/trunk/apiSiteVerificationService.php


import os, pprint, httplib2, sys, argparse

import google.oauth2.credentials, oauth2client
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client.file import Storage
from oauth2client import tools
from oauth2client.client import OAuth2WebServerFlow

pp = pprint.PrettyPrinter(indent=2)

SCOPES = ['https://www.googleapis.com/auth/siteverification']
API_SERVICE_NAME = 'siteVerification'
API_VERSION = 'v1'

def get_authenticated_service():
    storage = Storage('credentials.dat')
    credentials = storage.get()
    flow = oauth2client.client.flow_from_clientsecrets(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS', None), SCOPES)
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(flow, storage, tools.argparser.parse_args())
    http = httplib2.Http()
    http = credentials.authorize(http)
    return build(API_SERVICE_NAME, API_VERSION, http=http)

def list_ressources(service, **kwargs):
  results = service.webResource().list(**kwargs).execute()
  pp.pprint(results)

def get_token(service, **kwargs):
    return service.webResource().getToken(body=kwargs).execute()

def insert(service, **kwargs):
    return service.webResource().insert(**kwargs).execute()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(prog='PROG')
  subparsers = parser.add_subparsers(help='sub-command help', dest="cmd")
  p_list = subparsers.add_parser('list', help="list")

  p_getToken = subparsers.add_parser('token', help="get a token")
  p_getToken.add_argument('domains', metavar='domains', type=str, nargs='+', help='domains')

  p_verif = subparsers.add_parser('verify', help="trigger a verification")
  p_verif.add_argument('domains', metavar='domains', type=str, nargs='+', help='domains')
  p_verif.add_argument('--owners', nargs='?', help='Owners for this domain')
  args = parser.parse_args()

  os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
  service = get_authenticated_service()

  if args.cmd == 'list':
      list_ressources(service)
  elif args.cmd == 'token':
      for domain_name in args.domains:
          method, token = get_token(service, verificationMethod='DNS', site={ 'identifier': domain_name, 'type': 'INET_DOMAIN' }).values()
          print(domain_name, method, token)
  elif args.cmd == 'verify':
      for domain_name in args.domains:
          owners, sid, site = insert(service, verificationMethod='DNS', body={ 'owners': [], 'site': { 'identifier': domain_name, 'type': 'INET_DOMAIN' } }).values()
          print(owners, sid, site)

